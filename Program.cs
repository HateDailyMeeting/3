﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleApp13
{
    class SharedResource
    {
        public static int Count = 0;            // Наш общий ресурс
        public static Mutex Mtx = new Mutex();  // Объявляем Мьютекс
    }

    class Counter
    {
        public void Increment()
        {
       
          SharedResource.Count++;
        } 
        
        public void GetCount()
        {
            Console.WriteLine("Count = " + SharedResource.Count);
        }

    }

    class AnotherOne
    {
        int num;
        //public Thread OneThrd;

        public void RunAway(int n)
        {
            //num = n;
            Console.WriteLine("------------------------------------------");
            Console.WriteLine("Вошли каким то из потоков в метод RunAway");
            Console.WriteLine("------------------------------------------");
            //SharedResource.Mtx.WaitOne();
            Counter acnt = new Counter();
            num = n;
            do
            {
                //Thread.Sleep(300);
                Console.WriteLine("Shared res ++ in AnotherOne class");
                acnt.Increment();
                acnt.GetCount();
                num--;
                Console.WriteLine();

            } while (num > 0);

            //SharedResource.Mtx.ReleaseMutex();
        }
    }
    

        class NotMutexDemo
        {
        
        static List<Thread> threads = new List<Thread>();                   // Список потоков
        static int threadCount;// = int.Parse(Console.ReadLine());             // Число потоков
        static int IncrementCount;//= int.Parse(Console.ReadLine());          // Число потоков
        static ManualResetEvent startEvent = new ManualResetEvent(false);   // Событие для старта рабочих потоков
        static volatile int starterCount = 0;       // Счётчик запущенных потоков. volatile показывает, что переменная будет изменяться в различных потоках и её не надо оптимизировать
        static object LockObject = new object();    // Блокировка для изменения переменной starterCount

        static void Main()
            {

            Console.WriteLine("Введите количество потоков: ");
            threadCount = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите количество инкрементирования в каждом потоке: ");
            IncrementCount = int.Parse(Console.ReadLine());

            Console.WriteLine("_________________________________________");
            for (int i = 0; i < threadCount; i++)
            {
                Thread thread = new Thread(Work);
                threads.Add(thread);
                Console.WriteLine("Добавлен поток № " + i);
            }
            Console.WriteLine("_________________________________________");
            foreach (var thread in threads)
                new Thread(Starting).Start(thread);
                
            while (starterCount < threadCount) Thread.Sleep(1);

            Thread.Sleep(1);

            startEvent.Set();
            
           /* for (int i = 0; i < threadCount; i++)
            {
                Console.WriteLine(threads[i].IsAlive);
                Console.WriteLine(threads[i].ManagedThreadId);
                //threads[i].Join();
            }*/

            //Console.WriteLine("Press ENTER to quit");
           // Console.ReadKey();

            }


        static void Starting(object paramThread)
        {
            //Console.WriteLine("Starting " + SharedResource.Count);
            AnotherOne isnofear = new AnotherOne();
            
            lock (LockObject)
            {
                starterCount++;
                Console.WriteLine("LOCKED");
                isnofear.RunAway(IncrementCount);
            }
            //SharedResource.Mtx.WaitOne();
            startEvent.WaitOne();

            
            (paramThread as Thread).Start();
            Thread.Sleep(100);
            Console.ReadKey();
           // SharedResource.Mtx.ReleaseMutex();
        }

        static void Work()
        {
            return;
        }
    }
}

